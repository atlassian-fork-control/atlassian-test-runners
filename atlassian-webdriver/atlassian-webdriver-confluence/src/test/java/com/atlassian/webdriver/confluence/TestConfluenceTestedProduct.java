package com.atlassian.webdriver.confluence;

import com.atlassian.pageobjects.MultiTenantTestedProduct;
import com.atlassian.webdriver.testing.BaseTestProductTest;
import org.junit.Before;

public class TestConfluenceTestedProduct extends BaseTestProductTest {

    private static final String HOMEPAGE_PATH = "dashboard.action";

    private ConfluenceTestedProduct confluenceTestedProduct;

    @Before
    public void setup() {
        super.setup();
        confluenceTestedProduct = new ConfluenceTestedProduct(testerFactory, productInstance);
    }

    @Override
    protected void goToHomePage() {
        confluenceTestedProduct.gotoHomePage();
    }

    @Override
    protected String getHomePagePath() {
        return HOMEPAGE_PATH;
    }

    @Override
    protected MultiTenantTestedProduct getProduct() {
        return confluenceTestedProduct;
    }

}
