package com.atlassian.webdriver.jira;

import com.atlassian.pageobjects.Defaults;
import com.atlassian.pageobjects.MultiTenantTestedProduct;
import com.atlassian.pageobjects.Page;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.ProductInstance;
import com.atlassian.pageobjects.TestedProductFactory;
import com.atlassian.webdriver.utils.BaseUrlSupplierForHost;
import com.atlassian.pageobjects.binder.InjectPageBinder;
import com.atlassian.pageobjects.binder.PostInjectionProcessor;
import com.atlassian.pageobjects.binder.StandardModule;
import com.atlassian.pageobjects.component.Header;
import com.atlassian.pageobjects.elements.ElementModule;
import com.atlassian.pageobjects.elements.timeout.TimeoutsModule;
import com.atlassian.pageobjects.page.AdminHomePage;
import com.atlassian.pageobjects.page.HomePage;
import com.atlassian.pageobjects.page.LoginPage;
import com.atlassian.pageobjects.page.WebSudoPage;
import com.atlassian.webdriver.AtlassianWebDriverModule;
import com.atlassian.webdriver.jira.component.header.JiraHeader;
import com.atlassian.webdriver.jira.page.DashboardPage;
import com.atlassian.webdriver.jira.page.JiraAdminHomePage;
import com.atlassian.webdriver.jira.page.JiraLoginPage;
import com.atlassian.webdriver.jira.page.JiraWebSudoPage;
import com.atlassian.webdriver.pageobjects.DefaultWebDriverTester;
import com.atlassian.webdriver.pageobjects.WebDriverTester;

import java.util.Optional;

import static java.util.Objects.requireNonNull;

/**
 *
 */
@Defaults(instanceId = "jira", contextPath = "/jira", httpPort = 2990)
public class JiraTestedProduct implements MultiTenantTestedProduct<WebDriverTester>
{
    private final WebDriverTester webDriverTester;
    private final ProductInstance productInstance;
    private final PageBinder pageBinder;

    private String host;

    public JiraTestedProduct(final TestedProductFactory.TesterFactory<WebDriverTester> testerFactory, final ProductInstance productInstance)
    {
        this.productInstance = requireNonNull(productInstance, "productInstance can't be null");
        this.webDriverTester = testerFactory == null ? new DefaultWebDriverTester() : testerFactory.create();
        final BaseUrlSupplierForHost baseUrlSupplier = new BaseUrlSupplierForHost(productInstance.getBaseUrl(), () -> Optional.ofNullable(host));
        this.pageBinder = new InjectPageBinder(baseUrlSupplier, webDriverTester, new StandardModule(this), new AtlassianWebDriverModule(this),
                new ElementModule(), new TimeoutsModule(),
                binder -> binder.bind(PostInjectionProcessor.class).to(ClickableLinkPostInjectionProcessor.class));

        this.pageBinder.override(Header.class, JiraHeader.class);
        this.pageBinder.override(HomePage.class, DashboardPage.class);
        this.pageBinder.override(AdminHomePage.class, JiraAdminHomePage.class);
        this.pageBinder.override(LoginPage.class, JiraLoginPage.class);
        this.pageBinder.override(WebSudoPage.class, JiraWebSudoPage.class);
    }

    public DashboardPage gotoHomePage()
    {
        return pageBinder.navigateToAndBind(DashboardPage.class);
    }

    public JiraAdminHomePage gotoAdminHomePage()
    {
        return pageBinder.navigateToAndBind(JiraAdminHomePage.class);
    }

    public JiraLoginPage gotoLoginPage()
    {
        return pageBinder.navigateToAndBind(JiraLoginPage.class);
    }

    public <P extends Page> P visit(Class<P> pageClass, Object... args)
    {
        return pageBinder.navigateToAndBind(pageClass, args);
    }

    public PageBinder getPageBinder()
    {
        return pageBinder;
    }

    public ProductInstance getProductInstance()
    {
        return productInstance;
    }

    public WebDriverTester getTester()
    {
        return webDriverTester;
    }

    @Override
    public void setHost(String host) {
        this.host = host;
    }

}
