package com.atlassian.webdriver;

import com.atlassian.pageobjects.util.BrowserUtil;
import com.browserstack.local.Local;
import org.apache.commons.lang.StringUtils;

import java.util.Arrays;
import java.util.Date;
import java.util.Optional;

public class BrowserstackWebDriverFactory {
    private static final String[] BS_USER_KEYS = {"BROWSERSTACK_USER", "BROWSERSTACK_USERNAME"};
    private static final String[] BS_AUTH_KEYS = {"BROWSERSTACK_ACCESS_KEY", "BROWSERSTACK_ACCESSKEY", "BROWSERSTACK_KEY"};
    private static final String[] BUILD_KEYS = {"CI_BUILD_NUMBER", "BAMBOO_BUILD_NUMBER", "BITBUCKET_BUILD_NUMBER"};

    private static final String NAME = "browserstack";

    private BrowserstackWebDriverBuilder builder;

    public BrowserstackWebDriverFactory() {
        final Optional<String> username = findDefinedEnv(BS_USER_KEYS);
        final Optional<String> authkey = findDefinedEnv(BS_AUTH_KEYS);

        if (!username.isPresent()) {
            throw new IllegalStateException("Browserstack username must be set " +
                    "in one of the following environment variables: " + Arrays.toString(BS_USER_KEYS));
        }

        if (!authkey.isPresent()) {
            throw new IllegalStateException("Browserstack authentication key must be set " +
                    "in one of the following environment variables: " + Arrays.toString(BS_AUTH_KEYS));
        }

        builder = new BrowserstackWebDriverBuilder(username.get(), authkey.get())
                .withBrowser(System.getProperty("browserstack.browser"))
                .withBrowserVersion(System.getProperty("browserstack.browser.version"))
                .withOS(System.getProperty("browserstack.os"))
                .withOSVersion(System.getProperty("browserstack.os.version"))
                .withProjectName(System.getProperty("browserstack.project"))
                .withTestName(System.getProperty("browserstack.name"))
                .withBuildName(findDefinedEnv(BUILD_KEYS).orElseGet(() -> new Date().toString()));
    }

    public AtlassianWebDriver getDriver(Local local) throws Exception {
        final AtlassianWebDriver driver = builder
                .withLocalConnection(local)
                .withLocalIdentifier("atl-selenium_" + new Date().toInstant().toString())
                .build();
        BrowserUtil.setCurrentBrowser(driver.getBrowser());
        return driver;
    }

    public static boolean matches(String browserProperty) {
        return NAME.equalsIgnoreCase(browserProperty);
    }

    private static Optional<String> findDefinedEnv(String... keys) {
        for (String key : keys) {
            final String val = System.getenv(key);
            if (StringUtils.isNotBlank(val)) {
                return Optional.of(val);
            }
        }
        return Optional.empty();
    }
}
