package com.atlassian.webdriver.testing.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Allows to specify array of regexp string that will cause {@link com.atlassian.webdriver.testing.rule.JavaScriptErrorsRule} to omit js error when error is matched to at least one regexp from the array.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Inherited
public @interface JavaScriptErrorSkip {
    String[] value();
}
