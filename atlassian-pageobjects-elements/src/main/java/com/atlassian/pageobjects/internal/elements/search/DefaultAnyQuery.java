package com.atlassian.pageobjects.internal.elements.search;

import com.atlassian.annotations.Internal;
import com.atlassian.pageobjects.elements.search.AnyQuery;
import com.google.common.base.Supplier;

import javax.annotation.Nonnull;

@Internal
public final class DefaultAnyQuery<E> extends AbstractSearchQuery<E, AnyQuery<E>> implements AnyQuery<E>
{
    public DefaultAnyQuery(@Nonnull Supplier<Iterable<E>> querySupplier)
    {
        super(querySupplier);
    }

    @Nonnull
    @Override
    protected DefaultAnyQuery<E> newInstance(@Nonnull Supplier<Iterable<E>> supplier)
    {
        return newAnyQueryInstance(supplier);
    }

    @Nonnull
    @Override
    @SuppressWarnings("unchecked")
    protected <F> DefaultAnyQuery<F> newAnyQueryInstance(@Nonnull Supplier<Iterable<F>> supplier)
    {
        return pageBinder.bind(DefaultAnyQuery.class, supplier);
    }
}
